# 树莓派常用配置

## SSH开启

刚刚烧录完成后，拔插读卡器，在名为`boot`的盘符下，新建名为`ssh`的文件，不需要后缀。

用户名为：`pi`

默认密码为`raspberry`

## 连接WIFI

刚刚烧录完成后，拔插读卡器，在名为`boot`的盘符下，新建名为`wpa_supplicant.conf`的文件，添加如下

```bash
country=GB
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
	ssid="将我替换成第一个WiFi的名字，不删除引号,不能有中文"
	psk="将我替换成WiFi密码，不删除引号"
	priority=将我替换成数字，数字越大代表优先级越高
}
network={
	ssid="将我替换成第二个WiFi的名字，不删除引号,不能有中文"
	psk="将我替换成WiFi密码，不删除引号"
	priority=将我替换成数字，数字越大代表优先级越高
}
```

## 开启VNC

```bash
sudo raspi-config
```

![VNC01](./image/VNC01.png)

![VNC02](./image/VNC02.png)

![VNC03](./image/VNC03.png)

![VNC04](./image/VNC04.png)

![VNC05](./image/VNC05.png)

![VNC06](./image/VNC06.png)

## 静态IP设置

打开文件`/etc/dhcpcd.conf`

```bash
sudo vim /etc/dhcpcd.conf
```

找到

```bash
# Example static IP configuration:
#interface eth0
#static ip_address=192.168.1.51/24
#static ip6_address=fd51:42f8:caae:d92e::ff/64
#static routers=192.168.1.2
#static domain_name_servers=192.168.1.2
#static domain_name_servers=192.168.0.1 8.8.8.8 fd51:42f8:caae:d92e::1
```

根据实际需要进行修改，例如下面的配置。

```bash
# Example static IP configuration:
interface eth0									# 必须打开
static ip_address=192.168.1.51/24				  # 静态IP，后面的24不需要改
static routers=192.168.1.2						 # 网关
static domain_name_servers=192.168.1.2			  # DNS服务器
```

修改完成后必须重启

```bash
sudo reboot
```

## 修改SWAP分区大小

打开文件`/etc/dphys-swapfile`

```bash
sudo vim /etc/dphys-swapfile
```

找到`CONF_SWAPSIZE`根据需要修改

```bash
# /etc/dphys-swapfile - user settings for dphys-swapfile package
# author Neil Franklin, last modification 2010.05.05
# copyright ETH Zuerich Physics Departement
#   use under either modified/non-advertising BSD or GPL license

# this file is sourced with . so full normal sh syntax applies

# the default settings are added as commented out CONF_*=* lines

 
# where we want the swapfile to be, this is the default
#CONF_SWAPFILE=/var/swap

# set size to absolute value, leaving empty (default) then uses computed value
#   you most likely don't want this, unless you have an special disk situation
CONF_SWAPSIZE=1024		# 根据需要修改大小，单位MB

# set size to computed value, this times RAM size, dynamically adapts,
#   guarantees that there is enough swap without wasting disk space on excess
#CONF_SWAPFACTOR=2

# restrict size (computed and absolute!) to maximally this limit
#   can be set to empty for no limit, but beware of filled partitions!
#   this is/was a (outdated?) 32bit kernel limit (in MBytes), do not overrun it
#   but is also sensible on 64bit to prevent filling /var or even / partition
#CONF_MAXSWAP=2048

```

修改完成后重启

```bash
sudo reboot
```

查看是否修改成功

```bash
free -h
```

![SWAP01](./image/SWAP01.png)


