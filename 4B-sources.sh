#!/bin/sh

echo "Backup sources.list"
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak

echo "Modify source address"
sudo bash -c "cat > /etc/apt/sources.list" << EOF
deb http://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian/ buster main contrib non-free rpi
deb-src http://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian/ buster main contrib non-free rpi
EOF

echo "Backup raspi.list"
sudo cp /etc/apt/sources.list.d/raspi.list /etc/apt/sources.list.d/raspi.list.bak

echo "Modify source address"
sudo bash -c "cat > /etc/apt/sources.list.d/raspi.list" << EOF
deb http://mirrors.ustc.edu.cn/archive.raspberrypi.org/debian/ buster main
deb-src http://mirrors.ustc.edu.cn/archive.raspberrypi.org/debian/ buster main
EOF

echo "Update source"
sudo apt-get update
